"""
My first application
"""
import toga
from toga.style import Pack
from toga.style.pack import COLUMN, ROW

import speech
from threading import Thread
import os
import time

from http import HTTPStatus
import dashscope


def _reloop_index(_index, _array):
    _index += 1
    if _index >= len(_array):
        return 0
    return _index


class chatAI:
    def __init__(self, api_key):
        self.api_key = api_key
        dashscope.api_key = self.api_key

    def call_first(self):
        self.call_with_prompt("我是一个5岁的孩子,请给我讲一个pingping儿童学习的故事")

    def call_goon(self):
        self.call_with_prompt("我是一个5岁的孩子,请继续给我讲关于小朋友pingping学习的故事")

    def call_with_prompt(self, prompt):
        response = dashscope.Generation.call(
            model=dashscope.Generation.Models.qwen_turbo,
            prompt=prompt
        )
        # The response status_code is HTTPStatus.OK indicate success,
        # otherwise indicate request is failed, you can get error code
        # and message from code and message.
        if response.status_code == HTTPStatus.OK:
            print(response.output)  # The output text
            print(response.usage)  # The usage information
            self.outputText = response.output.text
        else:
            print(response.code)  # The error code.
            print(response.message)  # The error message.
    

class BeewarePing(toga.App):
    def __init__(self, formal_name=None):
        super().__init__(formal_name)
        self.speak_flag = False
        self.image_file_list = []
        self.image_file_index = 0
        self.image_text_list = ["这是一个关于pingping学习的故事", "pingping 他在一条路上走着", "pingping 是一个希望拥有生命的人"]
        self.image_text_index = 0
        file_path = os.path.abspath(os.path.dirname(__file__)) + "/resources/image"
        self.load_image(file_path) # Load images from resources folder
        self.DASHSCOPE_API_KEY = None

    def inputKey(self, widget):
        print(f"input key{widget.value}")
        self.DASHSCOPE_API_KEY = self.text_input.value
        self.charAI = chatAI(self.DASHSCOPE_API_KEY)
        self.speak_flag = True
        self.charAI.call_first()
        self.speak_flag = False

    def getImage_file(self):
        return self.image_file_list[self.image_file_index]
    
    def getImage_text(self):
        return self.image_text_list[self.image_text_index]

    def speak(self, message):
        self.speak_flag = True
        speech.say(message)
        self.speak_flag = False

    def load_image(self, file_dir):
        for file in os.listdir(file_dir):
            if file.endswith(".png") or file.endswith(".jpg") or file.endswith(".jpeg"):
                img_path = os.path.join(file_dir, file)
                self.image_file_list.append(img_path)


    
    def _image_file_reloop(self):
        self.image_file_index = _reloop_index(self.image_file_index, self.image_file_list)
        return self
    
    def _image_text_reloop(self):
        self.image_text_index = _reloop_index(self.image_text_index, self.image_text_list)
        return self
    
    def image_clicked(self, widget):
        if self.speak_flag:
            return
        if self.DASHSCOPE_API_KEY is None:
            Thread(target=self.speak, args=("请先输入阿里灵积Key，并输入回车按钮",)).start()
            return
        Thread(target=self.speak, args=(self.charAI.outputText,)).start()
        widget.icon = toga.Icon(self._image_file_reloop().getImage_file())
        self.charAI.call_goon() # Call chatAI to get a story

    def startup(self):
        main_box = toga.Box(style=Pack(direction=COLUMN))
        image_box = toga.Box()
        button = toga.Button(icon=toga.Icon(self.getImage_file()), on_press=self.image_clicked)
        Thread(target=self.speak, args=("请输入阿里灵积Key,并按回车键,之后可以点击图片听故事",)).start()
        # Add the image view widget to the main box
        image_box.add(button)

        text_box = toga.Box()
        self.text_input = toga.TextInput(style=Pack(width=512), on_confirm=self.inputKey)
        self.text_input.placeholder = "请输入阿里灵积Key,并按回车键"
        text_box.add(self.text_input)

        # 添加到 window
        self.main_window = toga.MainWindow(id='main', title=self.formal_name, \
                            position=(100, 100), size=(512, 512), \
                            resizable=True, minimizable=True, \
                            resizeable=None, closeable=None)
        
        main_box.add(image_box)
        main_box.add(text_box)
        self.main_window.content = main_box
        self.main_window.show()

def main():
    return BeewarePing()

