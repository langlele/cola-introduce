beeware ping
============

## 开始
briefcase dev

```bash
briefcase create windows
briefcase build windows
briefcase run windows
briefcase package windows
```

## 编译
编译需要执行一下步骤

1. 创建 windows
briefcase create windows

2. 构建
briefcase build windows

3. 赋值文件

3.1 因引用win32， 需要将 win32， 和 win32/lib 下的文件都复制到 app_packages

3.2 如果使用speech, 需要修改文件 speech.py  
中的 print propmt 为 print(propmt),
thread 修改为 _thread

4. 运行测试
briefcase run windows

5. 打包
briefcase package windows