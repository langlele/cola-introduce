# -*- coding: utf-8 -*-
"""
对话智能体，
调用百度的智能接口
"""
import erniebot
# from speak import Speak
# erniebot.api_type = 'aistudio'
# erniebot.access_token = '*****'
import logging
import threading

class Singleton(object):
    _instance_lock = threading.Lock()
    def __init__(self, cls):
        self._cls = cls
        self._instance = {}
        self.uniqueInstance = None
    
    def __call__(self, api_key, isTest=False):
        with self._instance_lock:
            if self.uniqueInstance is None:
                self.uniqueInstance = self._cls(api_key, isTest)
        return self.uniqueInstance

@Singleton
class CharAI(object):
    def __init__(self, api_key, isTest=False):
        erniebot.api_type = 'aistudio'
        erniebot.access_token = api_key
        self.erniebot = erniebot
        self.messages = []
        self.loop_index_max = 10
        self.loop_index_count = 0
        self.isTest = isTest

    def call_first(self):
        return self.call_with_prompt("请给我讲一个pingping儿童学习的故事")

    def call_goon(self):
        return self.call_with_prompt("请继续给我讲关于pingping学习的故事")

    def call_with_prompt(self, prompt):
        self.loop_index_count += 1
        if self.loop_index_count > self.loop_index_max:
            self.messages = []
        if self.isTest:
            return f'这是一个测试的故事{self.loop_index_count}'
        message = {'role': 'user', 'content': prompt}
        self.messages.append(message)
        logging.info(f'------{self.messages}---------')
        response = self.erniebot.ChatCompletion.create(
            model='ernie-3.5',
            messages=self.messages,
            system="你是一个5岁孩子的妈妈，有专业的故事知识库"
        )
        return response.get_result()

# if __name__ == '__main__':
#     charAI = CharAI('f6ad296c5d93d5738d65ab480c6e9d8e51a7015b')
#     print(charAI.call_first())