# -*- coding: utf-8 -*-
"""
文本转换成语音的
"""
import edge_tts
import asyncio

class Speaker(object):
    def __init__(self):
        self.voice = 'zh-CN-YunxiNeural'
        self.rate = '-4%'
        self.volume = '+0%'
        self.output_file = "output.mp3"
        self.output_file_prefix = 'output'
        self.output_file_suffix = '.mp3'
        self.engine = edge_tts
        self.speak_flag = False

    def get_output_file(self, index):
        self.output_file = f"{self.output_file_prefix}_{index}_{self.output_file_suffix}"
        return f"{self.output_file_prefix}_{index + 1}_{self.output_file_suffix}"

    async def speak(self, index, text):
        self.speak_flag = True
        communicate = self.engine.Communicate(text, voice=self.voice, rate=self.rate, volume=self.volume)
        await communicate.save(self.get_output_file(index))
        self.speak_flag = False

if __name__ == "__main__":
    speaker = Speaker()
    loop = asyncio.get_event_loop_policy().get_event_loop()
    try:
        loop.run_until_complete(speaker.speak(10,"你好，点击图片即可以开始听pingping的故事"))
    finally:
        loop.close()