# -*- coding: utf-8 -*-
from flask import Flask, Response, copy_current_request_context
from flask_restful import Resource, Api
from service import Service
from mylogging import mylogging
import threading

service = Service()
http_index = 0

app = Flask(__name__)
api = Api(app)


class HelloWorld(Resource):
    def get(self):
        return {"message": "Hello World!"}
        # global http_index
        # http_index += 1
        # logging.info(f"http_index####{http_index}####")
        # return Response(generate('output.mp3'), mimetype='audio/x-wav')
api.add_resource(HelloWorld, '/')

@app.route('/audiostart/info', methods=['GET'])
def startInfo_mp3():
    return Response(generate('output_0_start_info_.mp3'), mimetype='audio/x-wav')

def generate(story_path):
    with open(story_path, 'rb') as fmp3:
        data = fmp3.read(1024)
        while data:
            yield data
            data = fmp3.read(1024)


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


@app.route('/audio/<file_key>', methods=['GET'])
def stream_mp3(file_key):
    global http_index
    http_index += 1
    mylogging(f"http_index####{http_index}####")
    mylogging(f"####{file_key}####")
    @copy_current_request_context
    def service_generate():
        global service
        service.run(int(file_key))
    threading.Thread(target=service_generate).start()
    story_path = service.speaker.output_file
    mylogging(f"####{story_path}####")
    return app.response_class(generate(story_path), mimetype='audio/x-wav')

if __name__ == '__main__':
    app.run(debug=True)