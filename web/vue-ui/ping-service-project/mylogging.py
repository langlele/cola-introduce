# -*- coding: utf-8 -*-
"""
日志工具对象
"""
import logging
import inspect


def mylogging(msg, *args, **kwargs):
    logging.basicConfig(level=logging.INFO)
    # 获取调用者的帧信息
    frame = inspect.currentframe().f_back
    obj = frame.f_locals.get('self', None)
    method = frame.f_code.co_name

    # 如果存在对象，打印对象的类名
    if obj is not None:
        obj_name = obj.__class__.__name__
    else:
        obj_name = None

    if logging.getLogger().getEffectiveLevel() == logging.INFO:
        logging.info(f'Object: {obj_name}, Method: {method}, Message: {msg}', *args, **kwargs)