# -*- coding: utf-8 -*-
"""
智能体与speaker接口 的服务接口
"""
from charAI import CharAI
from speaker import Speaker
import asyncio
from mylogging import mylogging


class Service(object):
    def __init__(self):
        super().__init__()
        self.charAI = CharAI('f6ad296c5d93d5738d65ab480c6e9d8e51a7015b', '11')
        self.speaker = Speaker()

    def run(self, index):
        # p = Thread(target=generate_speech, args=(index, self.speaker, self.charAI))
        # p.start()
        generate_speech(index, self.speaker, self.charAI)
        return self.speaker.output_file


def generate_speech(index, speaker, charAI):
    if index <= 0:
        return speaker.output_file
    else:
        story_str = charAI.call_goon()
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    try:
        loop.run_until_complete(speaker.speak(index, story_str))
    finally:
        loop.close()
    mylogging(f"####{speaker.output_file}####")


# if __name__ == "__main__":
#     print(Service(1).run())