# -*- coding: utf-8 -*-
"""
文本转换成语音的
"""
import edge_tts
import miniaudio
import time
from threading import Thread
import asyncio

class Speaker():
    def __init__(self):
        self.voice = 'zh-CN-YunxiNeural'
        self.rate = '-4%'
        self.volume = '+0%'
        self.output_file = 'output.mp3'
        self.engine = edge_tts
        self.speak_flag = False

    async def async_speak(self, text):
        self.speak_flag = True
        communicate = self.engine.Communicate(text, voice=self.voice, rate=self.rate, volume=self.volume)
        await communicate.save(self.output_file)
        info = miniaudio.get_file_info(self.output_file)
        stream = miniaudio.stream_file(self.output_file)
        with miniaudio.PlaybackDevice() as device:
            device.start(stream)
            time.sleep(info.duration + 0.1)
        self.speak_flag = False

    def tb_speak(self, text):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        loop.run_until_complete(self.async_speak(text))
        loop.close()

    def speak(self, text):
        self.speak_flag = True
        Thread(target=self.tb_speak, args=(text,)).start()


# if __name__ == "__main__":
#     speak = Speak()
#     speak.speak("你好，这是一个关于pingping学习的故事")