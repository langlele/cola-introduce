# -*- coding: utf-8 -*-
"""
对话智能体，
调用百度的智能接口
"""
import erniebot
# from speak import Speak
# erniebot.api_type = 'aistudio'
# erniebot.access_token = '*****'


class CharAI:
    def __init__(self, api_key, speaker):
        erniebot.api_type = 'aistudio'
        erniebot.access_token = api_key
        self.erniebot = erniebot
        self.messages = []
        self.speaker = speaker
        self.isoutputing = False
        self.loop_index_max = 10
        self.loop_index_count = 0

    def call_first(self):
        self.call_with_prompt("请给我讲一个pingping儿童学习的故事")

    def call_goon(self):
        self.call_with_prompt("请继续给我讲关于pingping学习的故事")

    def call_with_prompt(self, prompt):
        if self.loop_index_count >= self.loop_index_max:
            self.messages = []
        message = {'role': 'user', 'content': prompt}
        self.messages.append(message)
        response = self.erniebot.ChatCompletion.create(
            model='ernie-3.5',
            messages=self.messages,
            system="你是一个5岁孩子的妈妈，有专业的故事知识库"
        )
        self.isoutputing = True
        print(response.get_result())
        self.speaker.speak(response.get_result())
        self.messages.append(response.to_message())
        self.isoutputing = False

# if __name__ == '__main__':
    # charAI = CharAI('f6ad296c5d93d5738d65ab480c6e9d8e51a7015b', Speak())
    # charAI.call_first()