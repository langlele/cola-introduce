# -*- coding: utf-8 -*-
"""
My first application
"""
import toga
from toga.style import Pack
from toga.style.pack import COLUMN, ROW

import os
from beewarepingapk.charAI import CharAI
from beewarepingapk.speaker import Speaker


def _reloop_index(_index, _array):
    _index += 1
    if _index >= len(_array):
        return 0
    return _index


class beewarepingapk(toga.App):
    def __init__(self, formal_name=None):
        super().__init__(formal_name)
        self.image_file_list = []
        self.image_file_index = 0
        file_path = os.path.abspath(os.path.dirname(__file__)) + "/resources/image"
        self.load_image(file_path) # Load images from resources folder
        self.ACCESS_API_KEY = None
        self.ACCESS_TIPS = "请输入百度千帆Key,并按回车键,之后可以点击图片听故事"
        self.speaker = Speaker()

    def _image_file_reloop(self):
        self.image_file_index = _reloop_index(self.image_file_index, self.image_file_list)
        return self

    def getImage_file(self):
        return self.image_file_list[self.image_file_index]

    def load_image(self, file_dir):
        for file in os.listdir(file_dir):
            if file.endswith(".png") or file.endswith(".jpg") or file.endswith(".jpeg"):
                img_path = os.path.join(file_dir, file)
                self.image_file_list.append(img_path)

    def image_clicked(self, widget):
        if self.speaker.speak_flag:
            #   正在播放中
            return
        if self.ACCESS_API_KEY is None:
            self.speaker.speak(self.ACCESS_TIPS)
            return
        self.speaker.speak(self.charAI.outputText)
        widget.icon = toga.Icon(self._image_file_reloop().getImage_file())
        self.charAI.call_goon() # Call chatAI to get a story

    def inputKey(self, widget):
        print(f"input key{widget.value}")
        self.ACCESS_API_KEY = self.text_input.value
        self.charAI = CharAI(self.ACCESS_API_KEY, self.speaker)
        self.charAI.call_first()

    def startup(self):
        main_box = toga.Box(style=Pack(direction=COLUMN))
        image_box = toga.Box()
        button = toga.Button(icon=toga.Icon(self.getImage_file()), on_press=self.image_clicked)
        # Add the image view widget to the main box
        image_box.add(button)
        # 提示 输入key
        self.speaker.speak(self.ACCESS_TIPS)
        text_box = toga.Box()
        self.text_input = toga.TextInput(style=Pack(width=512), on_confirm=self.inputKey)
        self.text_input.placeholder = self.ACCESS_TIPS
        text_box.add(self.text_input)

        # 添加到 window
        self.main_window = toga.MainWindow(id='main', title=self.formal_name, \
                            position=(100, 100), size=(512, 512), \
                            resizable=True, minimizable=True, \
                            resizeable=None, closeable=None)
        
        main_box.add(image_box)
        main_box.add(text_box)
        self.main_window.content = main_box
        self.main_window.show()
        

def main():
    return beewarepingapk()

