import pyautogui 
import time

# 每秒输入一个字符 'a'
for i in range(600000000):  # 持续一分钟
    pyautogui.typewrite('a', interval=1)  # 每秒输入一个 'a'
    time.sleep(1)  # 等待一秒a