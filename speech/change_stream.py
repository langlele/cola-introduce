import pyaudio
import numpy as np
import time
import librosa

# 初始化PyAudio对象
p = pyaudio.PyAudio()

RATE = 44100
# 设置音频输入流和输出流
input_stream = p.open(format=pyaudio.paFloat32, channels=1, rate=44100, input=True, frames_per_buffer=1024)
output_stream = p.open(format=pyaudio.paFloat32, channels=1, rate=44100, output=True, frames_per_buffer=1024, output_device_index=7)

# 定义变声函数
def pitch_shift(signal, shift=0):
    signal_shifted = librosa.effects.pitch_shift(signal, sr=RATE, n_steps=6)
    # 变声实现代码
    return signal_shifted

# 实时变声
while True:
    # 读取音频信号
    data = input_stream.read(1024)

    # 转换为numpy数组
    signal = np.frombuffer(data, dtype=np.float32)

    # 实时变声
    signal_shifted = pitch_shift(signal, shift=2)

    # 将处理后的信号输出
    output_stream.write(signal_shifted.tobytes())

# 关闭流和PyAudio对象
input_stream.stop_stream()
output_stream.stop_stream()
input_stream.close()
output_stream.close()
p.terminate()
