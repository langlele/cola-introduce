import pyaudio
p = pyaudio.PyAudio()
#print(p)
device_index = 0

for i in range(p.get_device_count()):
    device_info = p.get_device_info_by_index(i)
    print(i)
    print(device_info.get('name', ''))
    if device_info.get('name', '').find('i2s') != -1:
        print(device_info)
        device_index = device_info.get('index')

print('Selected device index: {}'.format(device_index))
