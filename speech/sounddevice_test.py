import sounddevice as sd
from scipy.io.wavfile import write
import requests
import os
import json
import time


UPLOAD_PATH = os.path.join(os.path.dirname(__file__),'voice')

# print(sd.query_devices())
# 设置设备为 虚拟声卡
sd.default.device[0] = 2

# 设置录音参数
fs = 44100  # 采样频率
seconds = 5  # 录音时间


# 录音
print(f"开始录音...{UPLOAD_PATH}")
myrecording = sd.rec(int(seconds * fs), samplerate=fs, channels=2)
sd.wait()  # 等待录音结束
print("录音结束.")

# start_time = time.time()

# # 保存录音到指定目录
# upload_file = os.path.join(UPLOAD_PATH, 'output.wav')
# write(upload_file, fs, myrecording)  # 将录音保存为 WAV 文件

# # 用open的方式打开文件，作为字典的值。file是请求规定的参数，每个请求都不一样。
# files = {'file': open(upload_file, 'rb')}

# # 请求的地址，这个地址中规定请求文件的参数必须是file
# url = 'http://127.0.0.1:5001/upload'
# # 用files参数接收
# res = requests.post(url, files=files)
# print(json.loads(res.text)) # 打印返回结果

# end_time = time.time()
# print("耗时: {:.2f}秒".format(end_time - start_time))
