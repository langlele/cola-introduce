
import librosa
import soundfile as sf
import numpy as np
from scipy.signal import resample
import os

UPLOAD_PATH = os.path.join(os.path.dirname(__file__),'voice')


def change_pitch(audio_path):
    # 加载音频
    y, sr = librosa.load(audio_path)
    # 改变音高
    y_hat = librosa.effects.pitch_shift(y, sr=sr, n_steps=6)
    return y_hat, sr


# 原始音频文件路径
audio_path = os.path.join(UPLOAD_PATH, 'output.wav')

# 改变音高
new_audio, new_sr = change_pitch(audio_path)

audio_output_path = os.path.join(UPLOAD_PATH, 'output_audio.wav')

# 假设 y_hat 是处理过的音频，sr 是采样率
sf.write(audio_output_path, new_audio, new_sr)