# 虚拟声卡安装

## 安装
解压后，管理员运行 exe
![安装介绍](./image/image1.png)

## 配置
1. 找到声音设置
![alt text](./image/image2.png)

2. 更多声音设置
![alt text](./image/image3.png)
3. 声音设置
   1. 播放设置
   ![alt text](./image/image4.png)
   2. 录制设置
   ![alt text](./image/image5.png)
   ```
   1. 选则 虚拟声卡
   2. 点击属性，打开属性配置面版
   3. 勾选 侦听
   4. 选则 侦听设备，此处选则 自己的扬声器，即实际使用的播放设备
   5. 确认虚拟声卡属性配置
   6. 确认声音设置
   ```



apt install ffmpeg