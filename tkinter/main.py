# -*- coding: utf-8 -*-
import tkinter as tk
from PIL import ImageTk, Image
import os
import time
import pyttsx3
from threading import Thread


class TextToSpeech:
    def __init__(self):
        self.engine = pyttsx3.init()
        self.engine.setProperty('rate', 150)
        self.engine.setProperty('volume', 0.7)

    def say(self, text):
        global speak_flag
        self.engine.say(text)
        self.engine.runAndWait()
        # self.engine.endLoop()
        self.engine.stop()
        speak_flag = False

speak_flag = True

def speak(message):
    global speak_flag
    speak_flag = True
    tts = TextToSpeech()
    tts.say(message)


file_path = os.path.abspath(os.path.dirname(__file__)) + "/image"


window = tk.Tk()  # 创建一个新的窗口
window_width = 800
window_height = 500
window.title("ping ping")
window.geometry(f"{window_width}x{window_height}")  # 设置窗口的大小为500x500


# Load the images
img_files = []
img_text = ["这是一个关于pingping学习的故事", "pingping 他在一条路上走着", "pingping 是一个希望拥有生命的人"]
image_width = int(window_width/2)
for file in os.listdir(file_path):
    if file.endswith(".png") or file.endswith(".jpg") or file.endswith(".jpeg"):
        img_path = os.path.join(file_path, file)
        img = Image.open(img_path).resize((image_width, image_width))
        img_files.append(ImageTk.PhotoImage(img))

if len(img_files) < 2:
    raise ValueError("There must be at least two images in the image folder")


# Create a container frame
container = tk.Frame(window)
container.pack()
container.config(width=window_width)




def reloop_index(_index, _array):
    _index += 1
    if _index >= len(_array):
        return 0
    return _index


def label_click(event):
    global speak_flag
    if speak_flag:
        return
    global image_index
    image_index = reloop_index(image_index, img_files)
    this_label = event.widget
    this_label.config(image=img_files[image_index])

    global img_text_index
    img_text_index = reloop_index(img_text_index, img_text)
    text = img_text[img_text_index]
    Thread(target=speak, args=(text,)).start()
    append_content(text)


def create_label(container, name, image, label_click):
    label = tk.Label(container, name=name, image=image)
    label.pack(side=tk.LEFT)
    label.bind("<Button-1>", label_click)
    return label


image_index = 0
img_text_index = 0
left_label = create_label(container, "left", img_files[image_index], label_click)
right_label = create_label(container, "right", img_files[image_index], label_click)


# Create a container frame
container_history = tk.Frame(window)
container_history.pack()

# Append content to the input box
def append_content(content):
    input_box.config(state=tk.NORMAL)
    input_box.delete("1.0", tk.END)
    for char in content:
        input_box.insert(tk.END, char)
        input_box.update()
        time.sleep(0.05)  # 暂停0.1秒
    input_box.insert(tk.END, "\n")
    input_box.config(state=tk.DISABLED)


# Create the input box
start_text = "您好！这是一个美丽的故事"
input_box = tk.Text(container_history, width=window_width, font=('Arial', 14))
input_box.pack(fill=tk.BOTH, expand=True)
input_box.insert(tk.END, f"{start_text}\n")
input_box.config(state=tk.DISABLED)

window.after(1000, lambda: Thread(target=speak, args=(start_text,)).start())
window.mainloop()
